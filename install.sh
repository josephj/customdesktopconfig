#!/bin/bash

#Install gala ppa
#if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/*|grep elementary-os; then
  #sudo apt-add-repository ppa:elementary-os/stable -y 
#fi

sudo apt update -y

for i in ansible git; do
  sudo apt-get install $i -y
done

#clone configuration to local machine
if cd ~/.customdesktopconfig; then
  git pull; else
  mkdir -p ~/.customdesktopconfig
  git clone https://gitlab.com/josephj/customdesktopconfig.git ~/.customdesktopconfig
fi

#Execute the ansible playbook
ansible-playbook ~/.customdesktopconfig/main.yml

sudo snap install discord
